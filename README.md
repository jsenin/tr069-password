# tr069-password

Starts a ACS/CWMP server for dump and update a CPE user and password using TR069.


Access to CPE control panel using a non privileged user, look for DNS static addresses, and set ACS hostname to your IP address. Ej. acs.masmovil.com 

## start server
```
python acs-server.py --username admin --password pass
```
or 
```
python3 acs-server.py --username admin --password pass
```

## ssl support 
Starting tornado with ssl support requires autosigned cert
Press enter in every question

```
openssl req -x509 -nodes -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365")
```


## Troubleshooting


### SSL not enabled

Log looks like this :

```
192.168.200.1 - - [05/Oct/2020 01:16:56] code 400, message Bad HTTP/0.9 request type ('\x16\x03\x01\x00j\x01\x00\x00f\x03\x03]^?O\x96Ý\x97\x92×Ü\x80þM\x98¥\x17\x8a\x9eã¡j¡ÛÛ')
192.168.200.1 - - [05/Oct/2020 01:16:56] "jf]^?OÝ×ÜþM</" 400 -
```
CPE has ssl enabled (https) and you dont. Please stop server and start it with `-ssl` 
